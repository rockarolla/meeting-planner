from django.db import models


class Country(models.Model):
    country_name = models.CharField(max_length=50)
    local_currency = models.CharField(max_length=50)
    creation_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.country_name
