from datetime import datetime
from django.http import HttpResponse
from django.shortcuts import render
from meetings.models import Meeting


def welcome(request):
    template_data = {
        "message": "Welcome to the demo application for meeting planner!",
        "meetingsCount": Meeting.objects.count()
    }
    return render(request, "website/welcome.html", template_data)


def date(request):
    return HttpResponse("this page was served at " + str(datetime.now()))


def about(request):
    return HttpResponse("I will master this thing")
