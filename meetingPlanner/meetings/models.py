from datetime import time
from django.db import models


class Room(models.Model):
    name = models.CharField(max_length=200)
    floorNumber = models.IntegerField()
    roomNumber = models.IntegerField()

    def __str__(self):
        return f"{self.name}: room {self.roomNumber} on floor {self.floorNumber}"


class Meeting(models.Model):
    title = models.CharField(max_length=200)
    date = models.DateField()
    startTime = models.TimeField(default=time(8))
    duration = models.IntegerField(default=1)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.title} at {self.startTime} on {self.date}"
